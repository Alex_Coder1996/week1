<?php
/**
 * Plugin Name: DA Event plugin
 * Description: Event plugin for web4pro
 * Author:      Dmitry Alekseev
 */

add_action( 'init', 'create_events_taxonomies' );

function create_events_taxonomies() {

	register_taxonomy(
		'location',
		'events',
		array(
			'hierarchical' => false,
			'labels'       => array(
				'name'                => _x( 'Locations', 'taxonomy general name' ),
				'singular_name'       => _x( 'Location', 'taxonomy singular name' ),
				'search_items'        => __( 'Search Locations' ),
				'all_items'           => __( 'All Locations' ),
				'parent_item'         => null,
				'parent_item_colon'   => null,
				'edit_item'           => __( 'Edit Location' ),
				'update_item'         => __( 'Update Location' ),
				'add_new_item'        => __( 'Add New Location' ),
				'new_item_name'       => __( 'New Location Name' ),
				'add_or_remove_items' => __( 'Add or remove Location' ),
				'menu_name'           => __( 'Locations' ),
			),
			'show_ui'      => true,
			'query_var'    => true,
			'rewrite'      => array( 'slug' => 'location' ),
		)
	);
}

function da_create_events_post_type() {
	register_post_type(
		'events',
		array(
			'labels'      => array(
				'name'          => __( 'Events' ),
				'singular_name' => __( 'Event' ),
			),
			'public'      => true,
			'has_archive' => true,
			'supports'    => array( 'title', 'editor', 'custom-fields' ),
			'rewrite'     => array( 'slug' => 'events' ),
		)
	);
}
add_action( 'init', 'da_create_events_post_type' );

add_action( 'add_meta_boxes', 'da_event_custom_fields', 1 );

function da_event_custom_fields() {
	add_meta_box( 'da_event_custom_fields', 'Event settings', 'da_custom_fields_box_func', 'events', 'normal', 'high' );
}

function da_custom_fields_box_func( $post ) {
	$da_event_status = get_post_meta( $post->ID, 'status', 1 );
	$da_event_date   = get_post_meta( $post->ID, 'event_date', 1 );
	wp_nonce_field('da_nonce_field', '_da_wp_nonce');
	echo '
		<p> Event status:
			<label>Free<input type="radio" name="extra[status]" value="free" ' . checked( $da_event_status, 'free', false ) . '></label>
			<label>Invitation<input type="radio" name="extra[status]" value="invitation" ' . checked( $da_event_status, 'invitation', false ) . '></label>
		</p>
		<p> Event date: 
			<input type="date" name="extra[event_date]" value="' . $da_event_date . '">
		</p>
	';
}

add_action( 'save_post', 'da_event_custom_fields_update', 0 );

function da_event_custom_fields_update( $post_id ) {

	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
		return $post_id;
	}

	if ( empty($_POST) || ! wp_verify_nonce( $_POST['_da_wp_nonce'], 'da_nonce_field' ) ){
		return $post_id;
	}

	if (
		empty( $_POST['extra'] )
		|| wp_is_post_autosave( $post_id )
		|| wp_is_post_revision( $post_id )
	) {
		return false;
	}

	$_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] );
	foreach ( $_POST['extra'] as $key => $value ) {
		if ( empty( $value ) ) {
			delete_post_meta( $post_id, $key );
			continue;
		}

		update_post_meta( $post_id, $key, $value );
	}

	return $post_id;
}


function register_da_events_widget() {
	register_widget( 'Da_events_widget' );
}

add_action( 'widgets_init', 'register_da_events_widget' );

require_once 'class-da-events-widget.php';

require_once 'events-widget-shortcode.php';