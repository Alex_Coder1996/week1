<?php

class Da_events_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'Da_events_widget',
			__( 'Events widget', ' text_domain' ),
			array( 'description' => __( 'Events widget from Dmitry Alekseev', 'text_domain' ) )
		);
	}
	public function widget( $args, $instance ) {
		$title         = apply_filters( 'widget_title', $instance['title'] );
		$events_count  = $instance['events_count'];
		$events_status = $instance['events_status'];
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

        if ( $events_status !== 'all' ) {
            $da_meta_query = array(
	            'relation'   => 'AND',
                array(
                    'key'     => 'status',
                    'value'   => $events_status,
                    'compare' => '=',
                ),
                array(
                    'key'     => 'event_date',
                    'value'   =>  date( "Y-m-d" ),
                    'compare' => '>=',
                ),
            );
        } else {
	        $da_meta_query = array(
		        array(
			        'key'     => 'event_date',
			        'value'   =>  date( "Y-m-d" ),
			        'compare' => '>=',
		        ),
	        );
        }

        $myposts = get_posts(
            array(
                'posts_per_page' => $events_count,
                'post_type'      => 'events',
                'orderby'        => 'event_date',
                'order'          => 'ASC',
                'meta_query'     => $da_meta_query,
            ),
        );

		foreach ( $myposts as $post ) {
			setup_postdata( $post );
            ?>
            <div class="article-elem">
                <a href="<?php the_permalink(); ?>"><?php echo esc_html( get_the_title( $post->ID ) ); ?></a>
                <p>
                    Post status: <?php echo get_post_meta( $post->ID, 'status', 1 );?>
                </p>
                <p>
                    Post date: <?php echo get_post_meta( $post->ID, 'event_date', 1 ); ?>
                </p>
            </div>
            <?php
		}
		wp_reset_postdata();
		echo $args['after_widget'];
	}
	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Events widget', 'text_domain' );
		}

		if ( isset( $instance['events_count'] ) ) {
			$events_count = $instance['events_count'];
		} else {
			// default events_count number is 1
			$events_count = 1;
		}

		if ( isset( $instance['events_status'] ) ) {
			$events_status = $instance['events_status'];
		} else {
			$events_status = 'all';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'events_count' ); ?>"><?php _e( 'Events count:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'events_count' ); ?>" name="<?php echo $this->get_field_name( 'events_count' ); ?>" type="text" value="<?php echo esc_attr( $events_count ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'events_status' ); ?>"><?php _e( 'Events status:' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'events_status' ); ?>" name="<?php echo $this->get_field_name( 'events_status' ); ?>">
				<option value="all" <?php echo ( esc_attr( $events_status ) === 'all' ) ? ' selected' : ''; ?>>All</option>
				<option value="free" <?php echo ( esc_attr( $events_status ) === 'free' ) ? ' selected' : ''; ?>>Free</option>
				<option value="invitation" <?php echo ( esc_attr( $events_status ) === 'invitation' ) ? ' selected' : ''; ?>>Invitation</option>
			</select>
		</p>
		<?php
	}
	public function update( $new_instance, $old_instance ) {
		$instance                  = array();
		$instance['title']         = ( ! empty( $new_instance['title'] ) ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['events_count']  = ( ! empty( $new_instance['events_count'] ) ) ? wp_strip_all_tags( $new_instance['events_count'] ) : '';
		$instance['events_status'] = ( ! empty( $new_instance['events_status'] ) ) ? wp_strip_all_tags( $new_instance['events_status'] ) : '';
		return $instance;
	}

}
