<?php

/* Add a shortcode for Events widget*/

function da_events_widget_shortcode( $atts ) {
	$atts = shortcode_atts(
		array(
			'title'         => 'Events widget',
			'events_count'  => 1,
			'events_status' => 'all',
		),
		$atts, 'da-events-widget'
	);

	ob_start();
	the_widget('Da_events_widget', $atts);
	$output = ob_get_clean();
	return $output;
}
add_shortcode('da-events-widget', 'da_events_widget_shortcode');